Program to adapt GDB's remote protocol to Libre-SOC's CPU as
well as generate a Finite State Machine suitable for
implementing GDB's protocol in hardware.

## Python Build Instructions

requires a recent version of Rust stable

```bash
cargo install maturin
git clone https://salsa.debian.org/Kazan-team/gdb-adaptor.git
cd gdb-adaptor
maturin build --cargo-extra-args=--features=python-extension
python3 -m pip install --user target/wheels/*.whl
```

alternatively, if in a python3 virtualenv:

```bash
git clone https://salsa.debian.org/Kazan-team/gdb-adaptor.git
cd gdb-adaptor
maturin develop --cargo-extra-args=--features=python-extension
```
or
```bash
git clone https://salsa.debian.org/Kazan-team/gdb-adaptor.git
cd gdb-adaptor
pip install .
```
