// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
pub mod ast;
pub mod interpreter;

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        assert_eq!(2 + 2, 4);
    }
}
