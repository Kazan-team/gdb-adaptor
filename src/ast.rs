// SPDX-License-Identifier: LGPL-2.1-or-later
// See Notices.txt for copyright information
use bstr::{BString, ByteSlice};
use serde::Serialize;
use std::ops::Range;

#[derive(Copy, Clone, Eq, PartialEq, Debug, Serialize)]
#[serde(transparent)]
pub struct StateId(pub usize);

#[derive(Copy, Clone, Eq, PartialEq, Debug, Serialize)]
pub struct ByteStringRef {
    pub start: usize,
    pub length: usize,
}

impl ByteStringRef {
    pub fn to_range(self) -> Range<usize> {
        self.start..(self.start + self.length)
    }
}

#[derive(Clone, Debug, Serialize)]
#[serde(tag = "operation")]
pub enum State {
    Return,
    Call {
        target_state: StateId,
        return_state: StateId,
    },
    WriteByteStringToGDB(ByteStringRef),
}

#[derive(Clone, Debug, Serialize)]
pub struct AST {
    pub states: Vec<State>,
    pub constants: BString,
}

#[derive(Clone, Debug, Serialize)]
pub struct ASTBuilder {
    ast: AST,
}

impl ASTBuilder {
    pub fn new() -> Self {
        ASTBuilder {
            ast: AST {
                states: vec![],
                constants: BString::default(),
            },
        }
    }
    pub fn into_ast(self) -> AST {
        self.ast
    }
    pub fn intern_byte_string(&mut self, value: impl AsRef<[u8]>) -> ByteStringRef {
        let value = value.as_ref().as_bstr();
        if let Some(start) = self.ast.constants.find(value) {
            return ByteStringRef {
                start,
                length: value.len(),
            };
        }
        for i in (0..value.len()).rev() {
            if self.ast.constants.ends_with(&value[..i]) {
                self.ast.constants.extend_from_slice(&value[i..]);
                let start = self.ast.constants.len() - value.len();
                debug_assert_eq!(self.ast.constants[start..].as_bstr(), value);
                return ByteStringRef {
                    start,
                    length: value.len(),
                };
            }
        }
        unreachable!()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_intern_byte_string() {
        fn do_intern(builder: &mut ASTBuilder, bytes: impl AsRef<[u8]>) -> ByteStringRef {
            let bytes = bytes.as_ref().as_bstr();
            let retval = builder.intern_byte_string(bytes);
            assert_eq!(builder.ast.constants[retval.to_range()].as_bstr(), bytes);
            retval
        }
        let mut builder = &mut ASTBuilder::new();
        assert_eq!(builder.ast.constants, b"".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"abc"),
            ByteStringRef {
                start: 0,
                length: 3,
            }
        );
        assert_eq!(builder.ast.constants, b"abc".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"ab"),
            ByteStringRef {
                start: 0,
                length: 2,
            }
        );
        assert_eq!(builder.ast.constants, b"abc".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"bc"),
            ByteStringRef {
                start: 1,
                length: 2,
            }
        );
        assert_eq!(builder.ast.constants, b"abc".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b""),
            ByteStringRef {
                start: 0,
                length: 0,
            }
        );
        assert_eq!(builder.ast.constants, b"abc".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"bcd"),
            ByteStringRef {
                start: 1,
                length: 3,
            }
        );
        assert_eq!(builder.ast.constants, b"abcd".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"d"),
            ByteStringRef {
                start: 3,
                length: 1,
            }
        );
        assert_eq!(builder.ast.constants, b"abcd".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"def"),
            ByteStringRef {
                start: 3,
                length: 3,
            }
        );
        assert_eq!(builder.ast.constants, b"abcdef".as_bstr());
        assert_eq!(
            do_intern(&mut builder, b"ghi"),
            ByteStringRef {
                start: 6,
                length: 3,
            }
        );
        assert_eq!(builder.ast.constants, b"abcdefghi".as_bstr());
    }

    #[test]
    fn test_serialize() {
        unimplemented!()
    }
}
